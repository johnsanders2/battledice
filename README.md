Battle Dice
-
A Risk dice roller, built as a Laravel package.

## Why use this?
Have you ever been playing Risk and needed to attack someone with a large amount of armies? It can take a while to roll that whole battle out. This handy little application can do the work for you and save you time.

## Installation
`composer require johnsanders2/battledice`

## How to use it
The package will set up a route `/battledice`. Visit the route and have fun battling.

e.g. http://localhost:8000/battledice

Enter the number of attacking armies, the number of defending armies, and the minimum amount of attacker armies at which to stop. The app will programmatically run the battle and tell you who's won and how many armies are left behind.

## State of this package
This package is still a prototype. There is no styling to speak of, and the results simply show how many armies are left behind on each side. I'll be updating it over time to add polish.

## Future enhancements
See our issues page.

https://bitbucket.org/johnsanders2/battledice/issues?status=new&status=open