<?php

namespace Johnsanders2\Battledice;

/**
 * Class Battle
 * @package Johnsanders2\Battledice
 */
class Battle
{
    /**
     * @var $attackingArmies integer The number of attacking armies
     */
    protected $attackingArmies;
    /**
     * @var $defendingArmies integer The number of defending armies
     */
    protected $defendingArmies;
    /**
     * @var $minRemainingAttackingArmies integer The minimum amount of attacker armies to leave behind
     */
    protected $minRemainingAttackingArmies;

    /**
     * Battle constructor.
     * @param $attackingArmies
     * @param $defendingArmies
     * @param $minRemainingAttackingArmies
     */
    public function __construct($attackingArmies, $defendingArmies, $minRemainingAttackingArmies)
    {
        $this->attackingArmies = $attackingArmies;
        $this->defendingArmies = $defendingArmies;
        $this->minRemainingAttackingArmies = $minRemainingAttackingArmies;
    }

    /**
     * @return BattleResult
     */
    public function battle()
    {
        while ($this->shouldKeepAttacking()) {
            $numAttackingDice = $this->getNumAttackingDice($this->attackingArmies);
            $numDefendingDice = $this->getNumDefendingDice($this->defendingArmies);

            // build a dice cup for the attacker and defender
            $attackerDiceCup = new DiceCup($numAttackingDice, 6);
            $defenderDiceCup = new DiceCup($numDefendingDice, 6);

            // roll each dice cup
            $attackerRolls = $attackerDiceCup->roll(SORT_DESC);
            $defenderRolls = $defenderDiceCup->roll(SORT_DESC);

            // track the losses for this battle
            $attackerLosses = 0;
            $defenderLosses = 0;

            // resolve the losses
            for ($round = 0; $round <= $numDefendingDice - 1; $round++) {
                if ($defenderRolls[$round] >= $attackerRolls[$round]) {
                    $attackerLosses--;
                } else {
                    $defenderLosses--;
                }
            }
            $this->attackingArmies += $attackerLosses;
            $this->defendingArmies += $defenderLosses;
        }

        return new BattleResult($this->attackingArmies, $this->defendingArmies);
    }

    public function shouldKeepAttacking()
    {
        // logic:
        //   The attacker must have more armies than the minimum they want to leave behind
        //   AND attacker must have at least 2 armies
        //   AND defender must have at least 1 army
        return $this->attackingArmies > $this->minRemainingAttackingArmies
            && $this->defendingArmies > 0
            && $this->attackingArmies >= 2;
    }

    /**
     * @param $attackingArmies
     * @return mixed
     */
    public function getNumAttackingDice($attackingArmies)
    {
        $numAttackerDice = min(3, $attackingArmies - 1);

        return $numAttackerDice;
    }

    /**
     * @param $defendingArmies
     * @return mixed
     */
    public function getNumDefendingDice($defendingArmies)
    {
        $numDefenderDice = min(2, $defendingArmies);

        return $numDefenderDice;
    }
}