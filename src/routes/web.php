<?php

Route::get(
    "battledice",
    "Johnsanders2\Battledice\BattlediceController@index")
    ->name("battledice.index")
    ->middleware('web');

Route::post(
    "battledice",
    "Johnsanders2\Battledice\BattlediceController@battle")
    ->name("battledice.battle")
    ->middleware('web');
