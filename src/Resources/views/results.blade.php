@extends('battledice::layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Battle Results</h2>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Attacking Armies Remaining</td>
                        <td>{{ $battleResult->getNumAttackerArmies() }}</td>
                    </tr>
                    <tr>
                        <td>Defending Armies Remaining</td>
                        <td>{{ $battleResult->getNumDefenderArmies() }}</td>
                    </tr>
                    </tbody>
                </table>
                <a href="{{ route('battledice.index') }}" class="btn btn-primary">
                    <i class="fa fa-chevron-left"></i>
                    Battle again
                </a>
            </div>
        </div>
    </div>
@endsection

