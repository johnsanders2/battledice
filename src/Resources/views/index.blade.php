@extends('battledice::layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-4">
                <h2>Battle Dice</h2>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="" method="post" class="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input
                                name="attacker_armies"
                                type="number"
                                placeholder="Attacker Army Size (min 2)"
                                class="form-control"
                                min="2"
                        />
                    </div>
                    <div class="form-group">
                        <input
                                name="defender_armies"
                                type="number"
                                placeholder="Defender Army Size (min 1)"
                                class="form-control"
                                min="1"
                        />
                    </div>
                    <div class="form-group">
                        <input
                                name="min_remaining_armies"
                                type="number"
                                placeholder="Min. Attacker Armies Remaining"
                                class="form-control"
                                min="1"
                        />
                    </div>
                    <button class="btn btn-primary btn-block">Battle!</button>
                </form>
            </div>
            <div class="col-md-7 col-lg-8">
                <h2>Instructions</h2>
                <p>
                    Use this Risk dice roller if you want to skip the time it takes to roll out large battles.
                </p>
                <p>
                    Enter the number of attacking armies, the number of defending armies, and how many attacker
                    armies you want to leave behind. Then battle.
                </p>
                <p>
                    You'll be presented with a results page which will show the outcome of each dice roll and
                    how many armies each side is left with.
                </p>
            </div>
        </div>
    </div>
@endsection
