<?php

namespace Johnsanders2\Battledice;

/**
 * Class DiceCup
 * @package Johnsanders2\Battledice
 */
class DiceCup
{
    /** @var $diceList Dice[] */
    protected $diceList = [];

    /**
     * DiceCup constructor.
     * @param int $numDice
     * @param int $sidesPerDice
     */
    public function __construct($numDice = 0, $sidesPerDice = 6)
    {
        for ($diceCount = 1; $diceCount <= $numDice; $diceCount++) {
            $this->add(new Dice($sidesPerDice));
        }
    }

    /**
     * @param null $sortDirection
     * @return array
     */
    public function roll($sortDirection = null)
    {
        $results = [];

        foreach ($this->diceList as $dice) {
            $results[] = $dice->roll();
        }

        switch ($sortDirection) {
            case SORT_ASC:
                asort($results);
                break;
            case SORT_DESC:
                arsort($results);
                break;
            default:
        }

        return $results;
    }

    /**
     * @param $dice
     * @return $this
     */
    public function add($dice)
    {
        $this->diceList[] = $dice;

        return $this;
    }

    /**
     * @param $diceId
     * @return $this
     */
    public function remove($diceId)
    {
        unset($this->diceList[$diceId]);

        return $this;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->diceList);
    }
}
