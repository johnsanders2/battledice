<?php

namespace Johnsanders2\Battledice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BattlediceController extends Controller
{
    public function index()
    {
        return view("battledice::index");
    }

    public function battle(Request $request)
    {
        $validatedData = $request->validate([
            'attacker_armies' =>      'required|numeric|min:2',
            'defender_armies' =>      'required|numeric|min:1',
            'min_remaining_armies' => 'required|numeric|min:1|lt:attacker_armies',
        ]);

        $attacker = $validatedData["attacker_armies"];
        $defender = $validatedData["defender_armies"];
        $minRemaining = $validatedData["min_remaining_armies"];

        $battle = new Battle($attacker, $defender, $minRemaining);
        $battleResult = $battle->battle();

        return view("battledice::results", compact('battleResult'));
    }
}
