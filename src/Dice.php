<?php

namespace Johnsanders2\Battledice;

class Dice
{
    const DEFAULT_NUMBER_OF_SIDES = 6;
    protected $sides;

    /**
     * Dice constructor.
     * @param $sides
     */
    public function __construct($sides = self::DEFAULT_NUMBER_OF_SIDES)
    {
        $this->sides = $sides;
    }

    /**
     * @return mixed
     */
    public function getSides()
    {
        return $this->sides;
    }

    /**
     * @param mixed $sides
     */
    public function setSides($sides): void
    {
        $this->sides = $sides;
    }

    public function roll()
    {
        return mt_rand(1, $this->sides);
    }
}
