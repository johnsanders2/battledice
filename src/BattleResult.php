<?php

namespace Johnsanders2\Battledice;

class BattleResult
{
    protected $numAttackerArmies;
    protected $numDefenderArmies;

    /**
     * BattleResult constructor.
     * @param $numAttackerArmies
     * @param $numDefenderArmies
     */
    public function __construct($numAttackerArmies, $numDefenderArmies)
    {
        $this->numAttackerArmies = $numAttackerArmies;
        $this->numDefenderArmies = $numDefenderArmies;
    }

    /**
     * @return mixed
     */
    public function getNumAttackerArmies()
    {
        return $this->numAttackerArmies;
    }

    /**
     * @param mixed $numAttackerArmies
     */
    public function setNumAttackerArmies($numAttackerArmies): void
    {
        $this->numAttackerArmies = $numAttackerArmies;
    }

    /**
     * @return mixed
     */
    public function getNumDefenderArmies()
    {
        return $this->numDefenderArmies;
    }

    /**
     * @param mixed $numDefenderArmies
     */
    public function setNumDefenderArmies($numDefenderArmies): void
    {
        $this->numDefenderArmies = $numDefenderArmies;
    }
}
