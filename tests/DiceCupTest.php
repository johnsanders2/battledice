<?php

declare(strict_types=1);

use Johnsanders2\Battledice\Dice;
use Johnsanders2\Battledice\DiceCup;
use PHPUnit\Framework\TestCase;

final class DiceTest extends TestCase
{
    public function testYouCanAddDiceToTheCup()
    {
        $diceCup = new DiceCup();

        $diceCup
            ->add(new Dice(6))
            ->add(new Dice(6))
            ->add(new Dice(6));

        $this->assertTrue($diceCup->count() === 3);
    }

    public function testYouCanBuildADiceCupOnInstantiation()
    {
        $numDice = 3;
        $sidesPerDice = 6;

        $diceCup = new DiceCup($numDice, $sidesPerDice);

        $this->assertTrue($diceCup->count() === 3);
    }

    public function testYouCanRemoveDiceFromTheCup()
    {
        $diceCup = new DiceCup();

        $diceCup
            ->add(new Dice(6))
            ->add(new Dice(6))
            ->add(new Dice(6));

        $diceCup->remove(0);

        $this->assertTrue($diceCup->count() === 2);
    }

    public function testIfYouRoll3DiceYouGet3Results()
    {
        $diceCup = new DiceCup();

        $diceCup
            ->add(new Dice(6))
            ->add(new Dice(6))
            ->add(new Dice(6));

        $results = $diceCup->roll();

        $this->assertTrue(count($results) === 3);
    }

    public function testYouCanSortRollResults()
    {
        $diceCup = new DiceCup();

        $diceCup
            ->add(new Dice(6))
            ->add(new Dice(6))
            ->add(new Dice(6));

        // sorted ascending
        $results = $diceCup->roll(SORT_ASC);
        $sortedResults = $results;
        asort($sortedResults);
        $this->assertSame($results, $sortedResults);

        // sorted descending
        $results = $diceCup->roll(SORT_DESC);
        $sortedResults = $results;
        arsort($sortedResults);
        $this->assertSame($results, $sortedResults);
    }
}
