<?php

declare(strict_types=1);

use Johnsanders2\Battledice\Dice;
use PHPUnit\Framework\TestCase;

final class DiceTest extends TestCase
{
    public function testManyRollsCreateEvenDistribution()
    {
        $sides = 6;
        $dice = new Dice($sides);
        $numTests = 1000000;
        $evenDistAmount = $numTests / $sides;
        $distVariance = $evenDistAmount * .05;
        $distLow = $evenDistAmount - $distVariance;
        $distHigh = $evenDistAmount + $distVariance;

        $rolls = [];
        for ($i = 1; $i <= $numTests; $i++) {
            $result = $dice->roll();
            if (!isset($rolls[$result])) $rolls[$result] = 0;
            $rolls[$result]++;
        }

        $isEvenDistribution = true;
        foreach ($rolls as $roll) {
            if ($roll < $distLow || $roll > $distHigh) {
                $isEvenDistribution = false;
                break;
            }
        }
        $this->assertTrue($isEvenDistribution);
    }

    public function testRollsNeverExceedDiceBounds()
    {
        $sides = 6;
        $dice = new Dice($sides);
        $numTests = 100000;
        $exceedsDiceBounds = false;

        for ($i = 1; $i <= $numTests; $i++) {
            $result = $dice->roll();
            if ($result < 1 || $result > $sides) {
                $exceedsDiceBounds = true;
                break;
            }
        }

        $this->assertFalse($exceedsDiceBounds);
    }
}
